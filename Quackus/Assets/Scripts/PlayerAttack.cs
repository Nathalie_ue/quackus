﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
using PRTwo;

namespace GCTHREEPROne
{
    //Script mit dem der Spieler schaden macht
    public class PlayerAttack : MonoBehaviour
    {
        private Player_Controls_Default controls;

        //Die Position von dem der Schaden ausgeht
        [SerializeField]
        private Transform attackPosition;

        //Die Gegner werden auf den selben Layer gelegt
        [SerializeField]
        private LayerMask whatIsEnemy;

        //In welchem Umkreis der Spieler schaden macht
        [SerializeField]
        private float attackRange;

        //Wie viel Schaden der Spieler macht
        [SerializeField]
        private int damage;

        //Wie viel Schaden der Spieler nach dem Powerup macht
        [SerializeField]
        private int damageAfterPowerUp;

        //Lebensanzeige
        //public weil von einem anderen Script darauf zugegriffen wird
        public Slider healthbarSlider;

        //Wie viele Leben der Spieler hat
        //public weil von einem anderen Script darauf zugegriffen wird
        public int health = 100;

        //Lebensanzeige
        //public weil von einem anderen Script darauf zugegriffen wird
        public TextMeshProUGUI healthText;

        //Variabel wie viel Schaden der Spieler nimmt
        private int damageTaken;

        [SerializeField] private AudioSource punchSource;
        [SerializeField] private AudioClip punchClip;

        //exposed properties, so any class can theoratically cause damage to the player
        public int DamageTakenPublic
        {
            get { return damageTaken; }
            set { damageTaken = value; }
        }

        //Array für die Gegner
        private Collider2D[] enemiesToDamage;

        //Das PowerUps des Levels
        [SerializeField]
        private GameObject powerUp;

        //Referenz zum GameManager Script
        private GameManager gameManager;

        //Der Tag vom GameManager wird im Inspektor angezeigt
        [SerializeField]
        private string gameManagerTag = "GameManager";

        //Referenz zum Highscore Script
        private Highscore highScore;

        //Der Tag vom Canvas
        [SerializeField]
        private string canvasTag = "Highscore";

        //main cam reference, so it doesnt has to be referened through camera.main all the time
        private Camera cam;

        
        private float originalOrthographicSize;


        private bool attackIsPressed;

        private void Awake()
        {
            controls = new Player_Controls_Default();
            controls.Enable();
            controls.Player.Interact.performed += ctx => attackIsPressed = true;

            //Den Variabeln werden die Komponente zugeschrieben
            //healthText = GameObject.FindGameObjectWithTag(canvasTag).GetComponentInChildren<TextMeshProUGUI>();
            //healthbarSlider = GameObject.FindGameObjectWithTag(canvasTag).GetComponentInChildren<Slider>();
            //highScore = GameObject.FindGameObjectWithTag(canvasTag).GetComponentInChildren<Highscore>();
            gameManager = GameObject.FindGameObjectWithTag(gameManagerTag).GetComponent<GameManager>();

            //Das Leben wird angezeigt
            healthText.text = health.ToString();
            
            //Der Slider wird auf 100 gestellt
            healthbarSlider.value = health;      
            
            cam = Camera.main;
            originalOrthographicSize = cam.orthographicSize;
        }

        private void OnEnable()
        {
            //subscribing to the corresponding event-delegate
            Message<PlayerDeathEvent>.Add(PlayerReset);
            Message<DamageEvent>.Add(TakeDamage);
            Message<OnExplosionEvent>.Add(ResetPunchPitch);
        }

        private void OnDisable()
        {
            //unsubscribing from the corresponding delegate
            Message<PlayerDeathEvent>.Remove(PlayerReset);
            Message<DamageEvent>.Remove(TakeDamage);
            Message<OnExplosionEvent>.Remove(ResetPunchPitch);
        }

        private void Update()
        {
            //Wenn Enter gedrückt wird
            if(attackIsPressed)
            {
                //Ein unsichbarer Kreis an gegebener Position
                enemiesToDamage = Physics2D.OverlapCircleAll(attackPosition.position, attackRange, whatIsEnemy);

                //Setting the combat event, only if enemies are indeed hit
                if (enemiesToDamage.Length != 0)
                {
                    punchSource.clip = punchClip;
                    punchSource.Play();
                    punchSource.pitch += 0.08f;
                    Message.Raise(new CombatEvent(gameObject.name));
                }

                //Wenn das Powerup nicht aufgehoben wurde, übergibt man den Standard Wert
                if (powerUp != null)
                {
                    //Allen Gegnern im Angriffsradius wird Schaden zugeschrieben
                    for (int i = 0; i < enemiesToDamage.Length; i++)
                    {
                        enemiesToDamage[i].GetComponent<Enemy>().Damage(damage);
                    }
                }
                else //Sonst übergibt man den neuen Wert
                {
                    for (int i = 0; i < enemiesToDamage.Length; i++)
                    {
                        enemiesToDamage[i].GetComponent<Enemy>().Damage(damageAfterPowerUp);
                    }
                }
            }

            if(attackIsPressed) attackIsPressed = false;    //reset every time after has been enabled
        }

        //Ein Gizmo wird in der Szene gezeichnet, damit man den Agriffsradius sehen kann
        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(attackPosition.position, attackRange);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            //Wenn der Spieler einen Gegner berührt, 
            //geht es in die TakeDamage Methode mit dem Angriffswert des Gegners
            if (collision.gameObject.tag == "Enemy")
            {
                //Der DamageTaken Variabel wird die Angriffskraft des Gegners zugeschrieben
                damageTaken = collision.gameObject.GetComponent<Enemy>().attackAmount;

                //The subscribed methods for taking Damage is executed
                Message.Raise(new DamageEvent(gameObject.name));
            }

            if (collision.gameObject.tag == "DeathZone")
            {
                //The subscribed methods will be executed
                Message.Raise(new PlayerDeathEvent(gameObject.name));
            }

        }

        //Wenn der Spieler Schaden nimmt
        private void TakeDamage(DamageEvent ev)
        {
            //Das Leben reduziert sich
            health -= damageTaken;

            //Der Slider wird dem Leben angepasst
            healthbarSlider.value = health;

            //Dem Text wird die Lebensanzahl zugeschrieben
            healthText.text = health.ToString();

            //Wenn der Spieler Keine Leben mehr hat
            if (health <= 0)
            {
                //Die health auf null, damit man nicht in den Minusbereich kommt
                health = 0;

                //Und es geht in die Die Methode
                Message.Raise(new PlayerDeathEvent(gameObject.name));
            }
        }

        private void PlayerReset(PlayerDeathEvent ev)       //changed playerReset to a message event
        {
              //Der Spieler wird zum Checkpoint zurück gesetzt
             transform.position = gameManager.lastCheckpointPosition;

             //Das Leben wird wieder auf 100 gesetzt
             health = 100;

             //Der Slider wird dem Leben angepasst
             healthbarSlider.value = health;

             //Dem Text wird die Lebensanzahl zugeschrieben
             healthText.text = health.ToString();

            //Der Highscore wird auf 0 zurück gesetzt
            PlayerPrefs.SetInt("HighscoreData", 0);

            Message.Raise(new LoadHighscoreEvent(gameObject.name));
        }

        //because explosions always occur after enemy death, we can reset the pitch here
        private void ResetPunchPitch(OnExplosionEvent ev)
        {
            punchSource.pitch = 1;
        }
       
    }
}