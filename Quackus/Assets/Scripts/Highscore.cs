﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.IO;
using DG.Tweening;
using System;

namespace PRTwo
{
    //Script für den Highscore
    public class Highscore : MonoBehaviour
    {
        //Variabel für die Text Komponente
        [SerializeField]
        private TextMeshProUGUI pointTextMesh;
        
        //Eine Variabel für den Text Tween
        [SerializeField]
        private Vector3 punch;
        
        //Variabel für den Highscore
        //public, weil von anderen Scripten darauf zugegriffen wird
        public int currentHighScore;

        //Der Canvas mit den Punkten
        private GameObject canvas;

        //Der Tag vom Canvas
        [SerializeField]
        private string canvasTag = "Highscore";
        private bool doesHighScoreDataExist;
        private ShowHighscore showHighscore;

        private int highscoreData;

        [SerializeField]
        private bool firstLevel;

        /// <summary>
        /// Subscribe to events always in OnEnable with the Message<T>.Add Method
        /// </summary>
        private void OnEnable()
        {
            Message<UpdateHighscoreEvent>.Add(OnUpdateHighscoreEvent);
            Message<LoadHighscoreEvent>.Add(OnLoadHighscoreEvent);
            Message<SaveHighscoreEvent>.Add(OnSaveHighscoreEvent);
        }

        /// <summary>
        /// Unsubscribe to events always in OnDisable with the Message<T>.Remove Method
        /// </summary>
        private void OnDisable()
        {
            Message<UpdateHighscoreEvent>.Remove(OnUpdateHighscoreEvent);
            Message<LoadHighscoreEvent>.Remove(OnLoadHighscoreEvent);
            Message<SaveHighscoreEvent>.Remove(OnSaveHighscoreEvent);
        }
        

        private void Start()
        {

            if(!firstLevel)
            {
                //The subscribed methods will be executed
                Message.Raise(new LoadHighscoreEvent(gameObject.name));
            }


            PlayerPrefs.SetInt("HighscoreData", highscoreData);
        }

        private void Update()
        {
            //Dem Text wird der Highscore zugeschrieben
            pointTextMesh.text = currentHighScore.ToString();
        }
        
        //Methode um Highscore hinzuzufügen
        public void AddHighScore(int score)
        {            
            //Dem Highscore wird der neue Score auf addiert
            currentHighScore += score;

            //The subscribed methods will be executed
            Message.Raise(new UpdateHighscoreEvent(currentHighScore));
        }

        private void OnUpdateHighscoreEvent(UpdateHighscoreEvent ev)
        {
            //Wenn ein Tween gerade spielt, wird dieser beendet
            if (DOTween.IsTweening(transform))
            {
                transform.DOComplete();
            }

            //Jedesmal wenn sich der Highscore erhöht, wird die Zahl getweent
            transform.DOPunchScale(punch, 0.25f);
        }

        //Methode um den Highscore zu speichern
        public void OnSaveHighscoreEvent(SaveHighscoreEvent ev) //----------war SaveHighScore()
        {
            Debug.Log("SAFE");

            //Wert wird gespeichert
            PlayerPrefs.SetInt("HighscoreData", currentHighScore);
        }        
               
        private void OnLoadHighscoreEvent(LoadHighscoreEvent ev)
        {
            //Variable gets the value in the PlayerPrefs
            currentHighScore = PlayerPrefs.GetInt("HighscoreData");

            //Dem Text wird der Highscore zugeschrieben
            pointTextMesh.text = currentHighScore.ToString();
        }
    }
}