﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRTwo
{
    public class Screenshake : MonoBehaviour
    {

        private void OnEnable()
        {
            Message<OnExplosionEvent>.Add(ShakeScreenExplo);
            Message<PickupItemEvent>.Add(ShakeScreenItem);
        }

        private void OnDisable()
        {
            Message<OnExplosionEvent>.Remove(ShakeScreenExplo);
            Message<PickupItemEvent>.Remove(ShakeScreenItem);
        }

        //"UsualExplosion" screenshake
        private void ShakeScreenExplo(OnExplosionEvent ev)
        {
            if (ev._Intensity)
            {
                StartCoroutine(Shake(0.05f, 0.4f));
            }
            else
            {
                StartCoroutine(Shake(0.025f, 0.2f));
            }
        }

        //Item screenshake
        private void ShakeScreenItem(PickupItemEvent ev)
        {
            StartCoroutine(Shake(0.035f, 0.3f));
        }

        public IEnumerator Shake(float duration, float magnitude)
        {
            Vector3 originalPos = transform.localPosition;

            float elapsed = 0.0f;

            while (elapsed < duration)
            {
                float x = Random.Range(-3f, 3f) * (magnitude);

                float y = Random.Range(-3f, 3) * (magnitude);

                transform.localPosition += new Vector3(x, y, 0);

                elapsed += Time.deltaTime;

                yield return null;

            }

            transform.localPosition = originalPos;

        }
    }

}