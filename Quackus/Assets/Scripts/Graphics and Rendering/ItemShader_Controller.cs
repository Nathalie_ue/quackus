﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class ItemShader_Controller : MonoBehaviour
{
    [SerializeField] private float effectSpeed = 1, timeBetweenPulses = 1, effectIntensity = 1;
    private float valueTracker; //this variable is used to track the 
    private void Start(){
        StartCoroutine(ExecutePulse());
    }

    private IEnumerator ExecutePulse(){
        float value = 0;
        while(value < 1){
            value += Time.deltaTime * (Mathf.Abs(effectIntensity) + 0.0001f);   //Mutliplications should not be 0(zero) due to the while-loop being infinite
            Shader.SetGlobalFloat("_Pulse", value); //Setting it as a global variable so it is always retrievable for any shader
            yield return new WaitForSeconds(0.01f * (1 / effectSpeed));
        }
        value = 0; 
        Shader.SetGlobalFloat("_Pulse", value); //Reset after its done

        yield return new WaitForSeconds(timeBetweenPulses);
        StartCoroutine(ExecutePulse()); //Coroutine Loops
    }
}
