﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PRTwo
{
    //Script für den GameManager
    public class GameManager : MonoBehaviour2
    {
        //Referenz zum Script
        private static GameManager instance;

        //Die Position vom letzten Checkpoint
        public Vector2 lastCheckpointPosition;

        //Wenn die instance existiert
        private void Awake()
        {
            //Wenn die instance leer ist, wird der Variabel das GameObject zugewiesen
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad(instance);
            }
            else //Ansonsten wirds zerstört
            {
                Destroy(gameObject);
            }
        }

        private void OnEnable()
        {
            Message<EnemyDeathEvent>.Add(SlowMotion);
            Message<PauseGameEvent>.Add(PauseGameEventChange);
            Message<ContinueGameEvent>.Add(ResumeGame);
        }

        private void OnDisable()
        {
            Message<EnemyDeathEvent>.Remove(SlowMotion);
            Message<PauseGameEvent>.Remove(PauseGameEventChange);
            Message<ContinueGameEvent>.Remove(ResumeGame);
        }

        private void Start()
        {
            Message.Raise(new LoadHighscoreEvent(gameObject.name));

            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
           if(scene.name == "MainMenu")
           {
                ResumeGameInstant();
           }
        }


        private float internTime;       //used to track current timescale
        private bool canScaleTimeBack = true;
        private void Update()
        {
            if(Time.timeScale > 0 && Time.timeScale < 1 && canScaleTimeBack)
            {
                //lerping but with a buffer for float inaccuracy
                internTime = SmoothLerpValue(internTime, 1, 0.2f, 0.2f);       
                
                //setting timescale and also fixedDeltaTime
                SetTime(internTime);
            }
        }


        //setting slowmotion properly
        private void SlowMotion(EnemyDeathEvent ev)
        {
            //if the enemy is a boss, the death will have more "intense" slowmotion           
            float? timeMultiplier = EnemyDeathEvent.IsBoss ? 0.1f : 0.15f;       
            SetTime(timeMultiplier.Value);
            internTime = Time.timeScale;
            StartCoroutine(EnableTimeScaling());
        }

        IEnumerator EnableTimeScaling()
        {
            canScaleTimeBack = false;
            yield return new WaitForSecondsRealtime(0.1f);
            canScaleTimeBack = true;
        }


        //for UI
        private void PauseGameEventChange(PauseGameEvent ev)
        {
            int? newTime = Time.timeScale == 0 ? 1 : 0;     // if not exactly zero, the game is running.
            SetTime(newTime.Value);
        }

        private void ResumeGame(ContinueGameEvent ev)
        {
            //not "instantly back" to game but small fade in
            SetTimeSmooth(0.2f);
        }

        private void ResumeGameInstant()
        {
            //resetting both time and interntime
            SetTimeSmooth(1);
        }

        private void SetTimeSmooth(float timeScale)
        {
            SetTime(timeScale);
            internTime = Time.timeScale;
        }
    }
}