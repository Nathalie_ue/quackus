﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace PRTwo
{
    //Script um eine neue Szene zu laden
    public class ButtonSceneloader : MonoBehaviour
    {
        //Szene, die geladen werden soll, wird über den Inspektor eingetragen
        [SerializeField]
        private string sceneToLoad;

        //Variabel für den button
        private Button button;

        private void Awake()
        {
            //Die Komponente wird der Variabel zugeschrieben
            button = GetComponent<Button>();
        }

        //Immer wenn das Object aktiviert wird
        private void OnEnable()
        {
            //onClick des Buttons über Script
            button.onClick.AddListener(LoadScene);
        }

        //Immer wenn ein Object deaktiviert wird
        private void OnDisable()
        {
            button.onClick.RemoveListener(LoadScene);
        }

        //Szene in sceneToLoad wird geladen
        private void LoadScene()
        {
            Message.Raise(new LoadSceneEvent(sceneToLoad));
        }
    }
}
