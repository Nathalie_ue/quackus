﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace PRTwo
{
    //Script um zum hauptmenü zurückzukehren
    public class ReturnToMainMenu : MonoBehaviour
    {
        //Referenz zum Highscore Script
        private Highscore highScore;
        
        //Variabel die den Namen der zuladenen Szene speichert
        [SerializeField]
        private string mainMenuSceneName;

        //Der Tag vom Canvas
        [SerializeField]
        private string canvasTag = "Highscore";

        private void Awake()
        {
            //Der Variabel wird die Komponente zugeschrieben
            //highScore = GameObject.FindGameObjectWithTag(canvasTag).GetComponentInChildren<Highscore>();
        }

        private void Update()
        {
            //Wenn Escape gedrückt wird, wird die SaveHighScore Methode aufgerufen
            //und die zugeschriebene Szene geladen
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Message.Raise(new SaveHighscoreEvent(gameObject.name));
                //highScore.SaveThisBitch();
                SceneManager.LoadScene(mainMenuSceneName);
            }
        }
    }
}