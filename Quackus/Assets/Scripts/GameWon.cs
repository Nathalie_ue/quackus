﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.SceneManagement;

namespace PRTwo
{
    //Script für den GameWon Screen
    public class GameWon : MonoBehaviour
    {
        //Auf welcher Position der GameWon Screen stehenbleiben soll
        [SerializeField]
        private float endValue;

        //Wie schnell sich der Tween bewegen soll
        [SerializeField]
        private float moveDuration;

        //Referenz zum Highscore Script
        [SerializeField]
        private Highscore highScore;
        
        //Der Tag vom Canvas
        [SerializeField]
        private string canvasTag = "Highscore";

        //Name des Hauptmenüs
        [SerializeField]
        private string mainMenuSceneName;

        //Referenz zum GameManager Script
        private GameManager gameManager;

        //Tagname
        [SerializeField]
        private string gameManagerTag = "GameManager";

        private void Awake()
        {
            //Der Variabel wird die Komponente zugewiesen
            //highScore = GameObject.FindGameObjectWithTag(canvasTag).GetComponentInChildren<Highscore>();
        }

        private void Start()
        {          
            //Das GameWon GameObjekt wird in der Mitte des Bildschirmes bewegt und
            //nach dem Tween wird die LoadMainMenu Methode aufgerufen
            transform.DOLocalMoveY(endValue, moveDuration)
                     .OnComplete(LoadMainMenu);
        }

        //Speichert den Highscore und lädt die zugewiesende Szene
        private void LoadMainMenu()
        {
            Message.Raise(new SaveHighscoreEvent(gameObject.name));
            
            SceneManager.LoadScene(mainMenuSceneName);

            //Dem Checkpoint wird der Levelanfang zugeschrieben
            gameManager.lastCheckpointPosition = new Vector2(0f, 0f);
        }
    }
}