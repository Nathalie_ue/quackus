﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using System.IO;
using GCTHREEPROne; //for player

namespace PRTwo
{
    public class Enemy : MonoBehaviour
    {
        //Die Lebensanzahl des Gegners
        [SerializeField]
        private int currentHealth;

        //Die Angriffskraft des Gegners
        //public, da von einer anderen Klasse auf diesen Wert zugegriffen wird
        public int attackAmount = 20;

        //Ob der Gegner lebt
        private bool isAlive;

        //Ob der Gegner sich bewegen soll
        [SerializeField]
        private bool isMoving;

        //Referenz zum PlayerAttack Script
        private PlayerAttack playerAttack;
        
        //Referenz zum Highscore Script
        private Highscore highScore;

        //Der Tag vom Canvas
        [SerializeField]
        private string canvasTag = "Highscore";

        //Der Wert, der dem Highscore dazugeschrieben wird
        [SerializeField]
        private int score;

        //Eine Variabel für den Gegner Tween
        [SerializeField]
        private Vector3 punch;

        //wie schnell sich die Gegner bewegen
        [SerializeField]
        private float walkSpeed;

        //wie weit sie gehen
        [SerializeField]
        private float walkDistance;

        //variabel für originale position der Gegner auf der X achse
        private float originalXposition;

        [SerializeField]
        private GameObject[] killEnemyEffects;

        //original position auf der X achse wird in der variabel gespeichert
        void Start()
        {            
            originalXposition = gameObject.transform.localPosition.x;

            //Der Variabel wird die Komponente zugeschrieben
            highScore = GameObject.FindObjectOfType<Highscore>();
        }


        //die position des objectes wird mit einer neuen + bewegung überschrieben
        void FixedUpdate()
        {
            if (isMoving)
            {
                transform.localPosition = new Vector3(originalXposition + ((float)Mathf.Sin(Time.realtimeSinceStartup * walkSpeed) * walkDistance), transform.localPosition.y , transform.localPosition.z);
            }
        }

        //to only execute an event once, a bool to check if the boss was hit before is necessary
        private bool firstHit;

        //Wenn der Gegner verletzt wird
        public void Damage(int damageAmount)
        {
            if(isBoss && !firstHit) 
            {
                Message.Raise(new BossFightMusicEvent(gameObject.name));
                firstHit = true;
            }
            //Dem Leben wird der Schaden abgezogen
            currentHealth -= damageAmount;
            
            //Wenn ein Tween gerade spielt, wird dieser beendet
            if (DOTween.IsTweening(transform))
            {
                transform.DOComplete();
            }

            //Jedesmal wenn sich der Gegner geschlagen wird, 
            //wird er zurückgeworfen
            transform.DOPunchPosition(punch, 0.25f);
                                

            //Wenn der Genger keine Leben mehr hat, wird er zerstört
            if (currentHealth <= 0)
            {
                //subscribing to the corresponding event-delegate
                OnKillEnemy();              

                //Die Methode wird aufgerufen und der Wert wird übergeben
                ProcessDestroyedEnemy(gameObject, true, score);                
            }
        }
        
        private void OnKillEnemy()
        {
            //raising the main event
            Message.Raise(new EnemyDeathEvent(this.gameObject, isBoss));

            //We randomly take one of the particle systems
            int randomIndex = Random.Range(0, killEnemyEffects.Length);

            //Ckeck if it exists
            if (killEnemyEffects[randomIndex] != null)
            {
                killEnemyEffects[randomIndex].transform.localPosition = gameObject.transform.localPosition;
                //Play the particle system
                killEnemyEffects[randomIndex].GetComponent<ParticleSystem>().Play();

                //when boss a intense explosion is necessary
                Message.Raise(new OnExplosionEvent(gameObject, isBoss));
            }

            Destroy(gameObject);
        }

        [SerializeField] bool isBoss;
        //Wenn dem Highscore Punkte hinzugefügt werden
        public void ProcessDestroyedEnemy(GameObject destroyedEnemy, bool raiseHighScore, int score)
        {
            //Wenn der Highscore erhöht erden soll, wird die AddHighScore Methode aufgerufen
            //und der Wert übertragen
            if (raiseHighScore)
            {
                highScore.AddHighScore(score);
            }

            if (isBoss)
            {
                Message.Raise(new BossFightMusicEvent(gameObject.name));     //ending boss music
                Message.Raise(new BossDeathEvent(gameObject.name));          //specific bossDeath events
            }
        }
    }
}