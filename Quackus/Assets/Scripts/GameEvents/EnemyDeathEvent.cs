﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRTwo
{
    public struct EnemyDeathEvent
    {
        //is the enemy a boss?
        private static bool isBoss;

        //only a getter is required
        public static bool IsBoss
        {
            get { return isBoss; }
        }

        public EnemyDeathEvent(GameObject gameObject, bool isABoss)
        {
            isBoss = isABoss;
        }
    }
}