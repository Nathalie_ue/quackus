﻿using UnityEngine;
using System.Collections;

namespace PRTwo
{
    public struct KillEnemyEvent
    {
        /// <summary>
        /// Overwriting the C# Constructor of the class to set the desired data
        /// when the event will be raised
        /// </summary>
        /// <param name="newHighscore"></param>
        public KillEnemyEvent(GameObject killedEnemy)
        {

        }
    }
}