﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PRTwo
{
    public struct LoadSceneEvent
    {
       public LoadSceneEvent(string sceneToLoad)
       {
            SceneManager.LoadScene(sceneToLoad, LoadSceneMode.Single);
       }
    }
}

