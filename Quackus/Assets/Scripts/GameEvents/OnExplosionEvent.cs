﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRTwo
{
    public struct OnExplosionEvent
    {
        //if true its intense, if false its allright i guess
        private static bool intensity_;

        public bool _Intensity
        {
            get { return intensity_; }
        }

        public OnExplosionEvent(GameObject gameObject, bool intensity)
        {
            intensity_ = intensity;
        }
    }
}