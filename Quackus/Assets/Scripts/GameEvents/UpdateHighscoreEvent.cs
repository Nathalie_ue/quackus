namespace PRTwo
{
    /// <summary>
    /// This is a simple class which will be utilized as an Event
    /// This class has also some data, we can pass around when the event is raised in order to
    /// further work with the values
    /// </summary>
    public struct UpdateHighscoreEvent
    {
        private int highscore;

        public int Highscore
        {
            get { return highscore; }
            set { highscore = value; }
        }

        /// <summary>
        /// Overwriting the C# Constructor of the class to set the desired data
        /// when the event will be raised
        /// </summary>
        /// <param name="newHighscore"></param>
        public UpdateHighscoreEvent(int newHighscore)
        {
            highscore = newHighscore;            
        }
    }
}