﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRTwo
{
    public class UIManager : MonoBehaviour2
    {
        [SerializeField] private GameObject pauseMenu;

        private void OnEnable()
        {
            Message<PauseGameEvent>.Add(ChangePauseMenuStatus);
            Message<ContinueGameEvent>.Add(ChangePauseMenuStatusContinue);
        }

        private void OnDisable()
        {
            Message<PauseGameEvent>.Remove(ChangePauseMenuStatus);
            Message<ContinueGameEvent>.Remove(ChangePauseMenuStatusContinue);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (pauseMenu.activeInHierarchy)
                {
                    Message.Raise(new ContinueGameEvent(gameObject.name));
                }
                else
                {
                    Message.Raise(new PauseGameEvent(gameObject.name));
                }
            }
        }

        private void ChangePauseMenuStatus(PauseGameEvent ev)
        {
            pauseMenu.SetActive(!pauseMenu.activeInHierarchy);
        }

        private void ChangePauseMenuStatusContinue(ContinueGameEvent ev)
        {
            pauseMenu.SetActive(!pauseMenu.activeInHierarchy);
        }

    }
}

