﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PRTwo
{
    //Script um das Spiel zu beenden
    public class QuitGame : MonoBehaviour
    {
        //Variabel für den button
        private Button button;

        //Der Variabel wird die Komponente zugeschrieben
        private void Awake()
        {
            button = GetComponent<Button>();
        }

        //Immer wenn das Object aktiviert wird
        private void OnEnable()
        {
            //Das onClick der Buttons, nur über Script statt Inspektor
            button.onClick.AddListener(ExitGame);
        }

        //Immer wenn das Object deaktiviert wird
        private void OnDisable()
        {
            button.onClick.RemoveListener(ExitGame);
        }

        //Das Spiel wird geschlossen
        private void ExitGame()
        {
            Application.Quit();

            //Wird in der Console ausgegeben, um zu sehen ob der Button funktioniert
            Debug.Log("Quit");
        }
    }
}