﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRTwo
{
    public class CameraManager : MonoBehaviour
    {
        //will be set to avoid referncing camera.main in update
        private Camera cam;

        private float originalOrthographicSize;

        void OnEnable()
        {
            Message<CombatEvent>.Add(ChangeOrthoSize);

            //Should be applied to maincam-Go but through this also works if this isnt the case
            try
            {
                cam = GetComponent<Camera>();
            }
            catch
            {
                cam = Camera.main;
            }

            originalOrthographicSize = cam.orthographicSize;
        }

        private void OnDisable()
        {
            Message<CombatEvent>.Remove(ChangeOrthoSize);
        }

        // Update is called once per frame
        void Update()
        {
            if (cam.orthographicSize != originalOrthographicSize) cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, originalOrthographicSize, 0.4f * Time.deltaTime);
        }

        private void ChangeOrthoSize(CombatEvent ev)
        {
            cam.orthographicSize -= originalOrthographicSize / 50;
        }
    }
}

