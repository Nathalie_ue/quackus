﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace PRTwo
{
    //Script, um die nächste Szene zu laden
    public class LoadLevel : MonoBehaviour
    {
        //Der Trigger, der den Szenenübergang auslöst
        [SerializeField]
        private GameObject levelTrigger;

        //Das Level, das geladen werden soll
        [SerializeField]
        private string levelToLoad;

        //Referenz zum GameManager Script
        private GameManager gameManager;
        
        //Tagname
        [SerializeField]
        private string gameManagerTag = "GameManager";

        private void Awake()
        {
            //Der Variabel wird die GameManager Komponente am GameManager zugewiesen
            gameManager = GameObject.FindGameObjectWithTag(gameManagerTag).GetComponent<GameManager>();
        }

        private void OnEnable()
        {
            Message<LoadSceneEvent>.Add(EndLevel);
        }

        private void OnDisable()
        {
            Message<LoadSceneEvent>.Remove(EndLevel);
        }

        //Beim Betreten des Triggers wird das nächste Level geladen
        private void OnTriggerEnter2D(Collider2D collision)
        {
            //including wrapped loadScene Method
            Message.Raise(new LoadSceneEvent(levelToLoad));
        }

        void EndLevel(LoadSceneEvent ev)
        {
            //The subscribed methods will be executed
            Message.Raise(new SaveHighscoreEvent(gameObject.name));

            //Dem Checkpoint wird der Levelanfang zugeschrieben
            gameManager.lastCheckpointPosition = new Vector2(0f, 0f);
        }
    }
}