﻿using UnityEngine;
using System.Collections;
using UnityEngine.Playables;
using PRTwo;

namespace GCTHREEPROne
{
    public class CutsceneTrigger : MonoBehaviour
    {
        [SerializeField]
        private PlayableDirector playableDirector;

        [SerializeField]
        private GameObject trigger;

        [SerializeField]
        private GameObject player;

        private string playerTag = "Player";

        private bool cutsceneStarted = false;

        private void Start()
        {
            //making sure the trigger is active at the start of the level
            trigger.SetActive(true);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag(playerTag))
            {
                //player stops moving
                player.GetComponent<PlayerMovement>().enabled = false;
                
                //Cutscene starts
                playableDirector.Play();
                cutsceneStarted = true;
            }
        }

        private void Update()
        {
            if(cutsceneStarted)
            {
                //When the timeline is not playing anymore
                if (playableDirector.state != PlayState.Playing)
                {
                    CutsceneFinished();
                }
            }
        }

        private void CutsceneFinished()
        {
            //trigger gets deactivated
            trigger.SetActive(false);

            //player can move again
            player.GetComponent<PlayerMovement>().enabled = true;
        }
    }
}