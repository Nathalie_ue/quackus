﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using GCTHREEPROne; //for player

namespace PRTwo
{
    //Script für die Checkpoints
    public class Checkpoints : MonoBehaviour
    {
        //Referenz zum GameManager Script
        private GameManager gameManager;

        //Refernz zum PlayerAttack Script
        private PlayerAttack playerAttack;

        //Tagname
        [SerializeField]
        private string gameManagerTag = "GameManager";

        //Tagname
        [SerializeField]
        private string playerTag = "Player";

        /// <summary>
        /// Subscribe to events always in OnEnable with the Message<T>.Add Method
        /// </summary>
        private void OnEnable()
        {
            Message<SaveCheckpointEvent>.Add(OnSaveCheckpointEvent);
        }

        /// <summary>
        /// Unsubscribe to events always in OnDisable with the Message<T>.Remove Method
        /// </summary>
        private void OnDisable()
        {
            Message<SaveCheckpointEvent>.Remove(OnSaveCheckpointEvent);
        }        

        private void Start()
        {
            //Der Variabel wird die GameManager Komponente am GameManager zugewiesen
            gameManager = GameObject.FindGameObjectWithTag(gameManagerTag).GetComponent<GameManager>();

            //Der Variabel wird die PlayerAttack Komponente am Spieler zugewiesen
            playerAttack = GameObject.FindGameObjectWithTag(playerTag).GetComponent<PlayerAttack>();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag(playerTag))
            {
                //Der Variabel wird der letzt aktivierte Checkpoint zugeschrieben
                gameManager.lastCheckpointPosition = transform.position;

                //The subscribed methods will be executed
                Message.Raise(new SaveCheckpointEvent(gameObject.name));                
            }
        }

        private void OnSaveCheckpointEvent(SaveCheckpointEvent ev)
        {
            //Der Spieler wird geheilt
            playerAttack.health = 100;

            //Leben weird der Anzeige übergeben
            playerAttack.healthbarSlider.value = playerAttack.health;
            playerAttack.healthText.text = playerAttack.health.ToString();
        }
    }
}