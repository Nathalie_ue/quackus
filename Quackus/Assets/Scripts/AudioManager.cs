﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRTwo
{
    public class AudioManager : MonoBehaviour2
    {
        [SerializeField] AudioClip[] explosionSFX;
        [SerializeField] AudioClip itemSFX;
        [SerializeField] AudioClip bossMusic;
        private AudioClip originalClip;     //original music
        [SerializeField] private AudioSource explosionSource;
        private AudioSource musicSource;

        private void Start()
        {
            musicSource = GetComponent<AudioSource>();
            originalClip = musicSource.clip;
        }

        private void OnEnable()
        {
            Message<OnExplosionEvent>.Add(PlayExplosionSFX); 
            Message<PickupItemEvent>.Add(PlayItemPickup); 
            Message<BossFightMusicEvent>.Set(ChangeToBossMusic);
        }

        private void OnDisable()
        {
            Message<OnExplosionEvent>.Remove(PlayExplosionSFX);
            Message<PickupItemEvent>.Remove(PlayItemPickup);
            Message<BossFightMusicEvent>.Remove(ChangeToBossMusic);
        }

        private void ChangeToBossMusic(BossFightMusicEvent ev)
        {
            musicSource.clip = bossMusic;
            musicSource.Play();
            Message<BossFightMusicEvent>.Set(ChangeToOriginalMusic);
        }

        private void ChangeToOriginalMusic(BossFightMusicEvent ev)
        {
            musicSource.clip = originalClip;
            musicSource.Play();
            Message<BossFightMusicEvent>.Set(ChangeToBossMusic);
        }

        private void PlayExplosionSFX(OnExplosionEvent ev)
        {
            //if intense index is 1 else 0
            int? index = ev._Intensity ? 1 : 0;

            //50 percent chance of having a "slow motion" esk explosion sound (MyPlay calculates Time.timescale changes)
            if (Chance(50))
            {
                MyPlay(explosionSource, explosionSFX[index.Value]);
            }
            else
            {
                explosionSource.clip = explosionSFX[index.Value];
                explosionSource.pitch = Random.Range(0.95f, 1.05f);
                explosionSource.Play();
            }
        }

        //Plays when picking up an item
        private void PlayItemPickup(PickupItemEvent ev)
        {
            MyPlay(explosionSource, itemSFX);
        }
    }

}
