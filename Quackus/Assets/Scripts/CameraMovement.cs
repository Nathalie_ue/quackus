﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRTwo
{
    //Script für die Camera
    public class CameraMovement : MonoBehaviour
    {
        //Die Begrenzung links
        [SerializeField]
        private float leftEdge;

        //Die Begrnzung rechts
        [SerializeField]
        private float rightEdge;

        //Die Begrenzung oben
        //Sollte den selben Wert haben, wie die Mitte der Kamera
        [SerializeField]
        private float upperEdge;

        //Der Spieler der Szene
        [SerializeField]
        private GameObject player;

        //Array für alle Hintergründe, die sich bewegen sollen
        [SerializeField]
        private Transform[] backgrounds;

        //Um die Hintergründe zu bewegen
        [SerializeField]
        private float[] parallaxScales;

        //Wie viel sich die Hintergründe bewegen sollen
        public float parallaxAmount = 2;

        //Referenz für das Tranform der Kamera
        private Transform mainCamera;

        //Vorherige Position der Kamera
        private Vector3 previousCameraPosition;
         
        //Der Variabel wird das Transform der Kamera zugeschrieben
        private void Awake()
        {            
            mainCamera = Camera.main.transform;
        }

        private void Start()
        {
            //Der Variabel wird die Position der Kamera zugeschrieben
            previousCameraPosition = mainCamera.position;

            //Dem Array werden die jeweiligen positionen zugeschrieben
            parallaxScales = new float[backgrounds.Length];
            
            for (int i = 0; i < backgrounds.Length; i++)
            {
                parallaxScales[i] = backgrounds[i].position.z * -1;
            }
        }

        void Update()
        {
            //Die CameraLock Methode wird aufgerufen
            CameraLock();

            //Für jeden Hintergrund
            for (int i = 0; i < backgrounds.Length; i++)
            {
                //Die Hintergründe setzten sich in die andere Richtung als die Kamera
                float parallax = (previousCameraPosition.x - mainCamera.position.x) * parallaxScales[i];

                //Die Target Position besteht aus der aktuellen Position mal dem parallax
                float backgroundTargetPositionX = backgrounds[i].position.x + parallax;

                //Eine neue Position bestehend aus der Target position und den jeweiligen Positionen auf der Y und Z Achse
                Vector3 backgroundTargetPosition = new Vector3(backgroundTargetPositionX, backgrounds[i].position.y, backgrounds[i].position.z);

                //Der Fade zwischen der aktuellen und der target Position
                backgrounds[i].position = Vector3.Lerp(backgrounds[i].position, backgroundTargetPosition, parallaxAmount * Time.deltaTime);
            }

            //Der Variabel wir die neue Kamera position zugeschrieben
            previousCameraPosition = mainCamera.position;
        }

        //Methode für die Begrenzungen der Kamera
        private void CameraLock()
        {
            //Wenn der Spieler innerhalb der Begrenzung ist, lockt sich die Kamera an ihn 
            if (player.transform.position.x < rightEdge && player.transform.position.x > leftEdge)
            {
                mainCamera.position = new Vector3(player.transform.position.x, mainCamera.position.y, mainCamera.position.z);
            }

            //Wenn der Spieler die Begrenzung nach oben überschreitet, folgt die Kamera ihn
            if (player.transform.position.y > upperEdge)
            {
                mainCamera.position = new Vector3(player.transform.position.x, player.transform.position.y, mainCamera.position.z);
            }

            //Wenn der Spieler die Begrenzung rechts/links überschreitet, wird die Kamera an die Begrenzung gelockt
            if (player.transform.position.x > rightEdge)
            {
                mainCamera.position = new Vector3(rightEdge, mainCamera.position.y, mainCamera.position.z);
            }
            else if (player.transform.position.x < leftEdge)
            {
                mainCamera.position = new Vector3(leftEdge, mainCamera.position.y, mainCamera.position.z);
            }

            //Wenn der Spieler die Begrenzung rechts/links und oben überschreitet, wird die Kamera an die Begrenzung gelockt
            //aber folgt dem Spieler auf der Y Achse
            if (player.transform.position.x > rightEdge && player.transform.position.y > upperEdge)
            {
                mainCamera.position = new Vector3(rightEdge, player.transform.position.y, mainCamera.position.z);
            }
            else if (player.transform.position.x < leftEdge && player.transform.position.y > upperEdge)
            {
                mainCamera.position = new Vector3(leftEdge, player.transform.position.y, mainCamera.position.z);
            }
        }
    }
}
