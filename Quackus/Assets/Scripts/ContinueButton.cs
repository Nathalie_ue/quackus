﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PRTwo
{
    public class ContinueButton : MonoBehaviour
    {
        
        private Button button;

        
        private void Awake()
        {
            button = GetComponent<Button>();
        }

    
        private void OnEnable()
        {
            
            button.onClick.AddListener(ContinueGameEvent);
        }

        
        private void OnDisable()
        {
            button.onClick.RemoveListener(ContinueGameEvent);
        }

        
        private void ContinueGameEvent()
        {
            Message.Raise(new ContinueGameEvent(gameObject.name));
        }
    }
}

