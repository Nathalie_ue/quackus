﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GCTHREEPROne;


namespace PRTwo
{
    //Script für den Bosskampf, an den Spieler hängen!
    public class Boss : MonoBehaviour
    {
        //Die erste Bossphase
        [SerializeField]
        private GameObject firstBoss;

        //Die zweite Bossphase
        [SerializeField]
        private GameObject secondBoss;

        //Das PowerUp
        [SerializeField]
        private GameObject powerUp;

        //Referenz zum PlayerMovement Script
        private PlayerMovement playerMovement;

        //Der Sprite Renderer des Spielers
        private SpriteRenderer spriteRenderer;

        //Der Text, der angezeigt werden soll
        [SerializeField]
        private GameObject shownText;

        //Der Text für die Schleife
        [SerializeField]
        private GameObject bowText;
                   
        //Das GameObject mit dem GameWon Screen
        [SerializeField]
        private GameObject gameWonScreen;

        //Ob der Boss existiert
        private bool firstBossIsAlive;

        private void Start()
        {
            //bool wird auf true gestellt;
            firstBossIsAlive = true;

            //Das PowerUp wird deaktiviert
            powerUp.SetActive(false);

            //Der Text wird deaktiviert
            bowText.SetActive(false);

            //Der Text wird deaktiviert
            shownText.SetActive(false);

            //Der GameWon Screen wird deaktiviert
            gameWonScreen.SetActive(false);

            //Der Variabel wird das PlayerMovement Script zugewiesen
            playerMovement = GetComponent<PlayerMovement>();

            //Die SpriteRenderer Komponente am Spieler wird der Variabel zugeschrieben
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

       

        private void OnEnable()
        {
            Message<BossDeathEvent>.Set(FirstBossDeath);
        }

        private void FirstBossDeath(BossDeathEvent ev)
        {
            //Das PowerUp der Szene wird aktiviert
            powerUp.SetActive(true);

            //Der Text wird aktiviert
            bowText.SetActive(true);

            //Das Sprite wird geändert
            spriteRenderer.sprite = playerMovement.firstSprite;

            //Der Text wird aktiviert
            shownText.SetActive(true);

            //setting this up for "next time"
            Message<BossDeathEvent>.Set(SecondBossDeath);

            Message.Raise(new SaveCheckpointEvent(gameObject.name));
        }

        private void SecondBossDeath(BossDeathEvent ev)
        {
            //Der GameWon Screen wird aktiviert
            gameWonScreen.SetActive(true);
        }

    }
}
