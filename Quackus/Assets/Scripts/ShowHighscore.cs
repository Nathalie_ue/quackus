﻿using System;
using System.IO;
using TMPro;
using UnityEngine;

namespace PRTwo
{
    //Script, dass den Highscore in der Highscore Szene anzeigt
    public class ShowHighscore : MonoBehaviour
    {
        //Text, der angezeigt wird, wenn es noch keinen Highscore gibt
        [SerializeField]
        private string noHighScoreMessage;
        
        //Text Komponente, die den Highscore anzeigt
        [SerializeField]
        private TextMeshProUGUI highScoreTextMesh;

        //Text Komponente, die den höchsten Highscore anzeigt
        [SerializeField]
        private TextMeshProUGUI maxHighScoreTextMesh;
        
        //Der geladene Highscore
        private int loadedHighScore;

        //Der beste Highscore
        private int bestHighScore;
        
        //Ob die SaveFile für den Highscore existiert
        private bool doesHighScoreDataExist;
        
        //Die Highscore Datei, die angezeigt werden soll
        //private HighscoreData highScoreData;
        
        //Der Dateipfad des Highscores
        private string filePath;
        

        private void Awake()
        {
            if (PlayerPrefs.HasKey("HighscoreData"))
            {
                LoadHighScore();
            }
            else
            {
                NoHighscore();
            }
        }

        private void NoHighscore()
        {
            highScoreTextMesh.text = noHighScoreMessage;

            maxHighScoreTextMesh.text = noHighScoreMessage;
        }

        //Methode wenn der Highscore geladen werden soll
        public void LoadHighScore()
        {
            loadedHighScore = PlayerPrefs.GetInt("HighscoreData");

            highScoreTextMesh.text = loadedHighScore.ToString();

            //Wenn eine Speicherdatei in Playerprefs existiert, 
            //wird der Variabel der Int zugeschrieben und dann angezeigt
            if (PlayerPrefs.HasKey("Highscore"))
            {
                bestHighScore = PlayerPrefs.GetInt("Highscore");

                //Wenn der Int in bestHighScore kleiner als der neue ist,
                //wird der Variabel der neue Wert übertragen und gespeichert
                if (bestHighScore < loadedHighScore)
                {
                    bestHighScore = loadedHighScore;
                    PlayerPrefs.SetInt("Highscore", bestHighScore);
                }

                maxHighScoreTextMesh.text = bestHighScore.ToString();
            }
            else
            {
                bestHighScore = loadedHighScore;
                PlayerPrefs.SetInt("Highscore", bestHighScore);

                maxHighScoreTextMesh.text = bestHighScore.ToString();
            }
        }
    }
}